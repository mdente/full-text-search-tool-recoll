import os
import json
import gspread
from unicodedata import normalize
from dateutil import parser

from pprint import pprint
from oauth2client.service_account import ServiceAccountCredentials

from decouple import config

FILE_LOCATION = config('LANDRI_ACERVO_REDALINT_ROOTDIR') # Não colocar a / no final

def find(name, path):
    for root, dirs, files in os.walk(FILE_LOCATION):
        if name in files:
            return os.path.join(root, name)

def touch_datatime(arquivo, ano_publicacao):
    try:
        ts = parser.parse(ano_publicacao)
        isostr = ts.strftime('%Y%m%d')
        cmd = 'touch "{}" -t {}0000'.format(arquivo, isostr)
        print(cmd)
        os.system(cmd)
    except:
        raise

def exiftool_autores(autores, arquivo):
    if len(autores) > 0:
        os.system('exiftool -author="{}" "{}"'.format(autores, arquivo))

def download_extra_metadata():
    scope = ['https://spreadsheets.google.com/feeds',
            'https://www.googleapis.com/auth/drive']

    creds = ServiceAccountCredentials.from_json_keyfile_name('/redalint/client_secret.json', scope)
    client = gspread.authorize(creds)

    sheet = client.open("teste-redalint").sheet1

    list_of_hashes = sheet.get_all_records()

    for idx, hash_ in enumerate(list_of_hashes, start=0):
        list_of_hashes[idx] = {
            normalize('NFKD', str(k)): normalize('NFKD', str(v))
            for k, v in hash_.items()
        }

    middle_json = {dict_result["nome_arquivo"]: dict_result for dict_result in list_of_hashes}

    for arquivo in middle_json.keys():
        autores_list = middle_json[arquivo]["autores"].split(";")
        nacionalidade_list = middle_json[arquivo]["nacionalidade"].split(";")
        areas_list = middle_json[arquivo]["area_formacao"].split(";")
        instituto_list = middle_json[arquivo]["instituicao_atuacao"].split(";")
        depto_list = middle_json[arquivo]["instituicao_atuacao_depto"].split(";")

        middle_json[arquivo]["autores_formatados"] = [list(x) for x in list(zip(autores_list, nacionalidade_list, areas_list, instituto_list, depto_list))]
        ano_publicacao = middle_json[arquivo].get("ano_publicacao")
        autores = middle_json[arquivo].get("autores", [])

        fullpath = find(arquivo, FILE_LOCATION)

        if fullpath is None:
            print('[FAIL]', arquivo)
            continue

        print('[OK]', fullpath)
        touch_datatime(fullpath, ano_publicacao)
        # exif -> nao funciona para PDFs (aparentemente)
        #exiftool_autores(autores, fullpath)

    with open("/redalint/metadados.json", "w") as f:
        json.dump(middle_json, f, indent=4, sort_keys=True, ensure_ascii=False)

    return True

if __name__ == "__main__":
    download_extra_metadata()