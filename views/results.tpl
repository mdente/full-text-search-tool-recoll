%include header title=": " + query['query']+" ("+str(nres)+")"
%include search query=query, dirs=dirs, sorts=sorts
<div id="status">
    <div id="found">
        Encontrados <b>{{nres}}</b> resultados na busca de: <b><i>{{qs}}</i></b>
        <small class="gray">({{time.seconds}}.{{time.microseconds/10000}}s)</small>
    </div>
    <br style="clear: both">
</div>
%include pages query=query, config=config, nres=nres

<div class="container"><div class="row"><div id="results">
%for i in range(0, len(res)):
    %include result d=res[i], i=i, query=query, config=config,
%end
</div></div></div>
%include pages query=query, config=config, nres=nres
%include footer
<!-- vim: fdm=marker:tw=80:ts=4:sw=4:sts=4:et:ai
-->
