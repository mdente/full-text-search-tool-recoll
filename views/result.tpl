%import shlex, unicodedata, os
<div class="search-result">
    <div class="search-result-title" id="r{{d['sha']}}" title="{{d['abstract']}}">
        <a href="{{d['url_formatada']}}" style="color: #14487A;">
            <h3 style="font-size: 20px; margin-bottom: 10px; margin-top: 30px;">
                %if not d['titulo']:
                    {{d['label']}}
                %else:
                    {{d['titulo']}}
                %end
            </h3>
        </a>
    </div>
    <hr/>
    %if len(d['autores']) > 0:
        <div class="search-result-author" style="color:#000;">Autor(es): {{d['autores']}}</div>
    %end
    %if d['tipo_publi']:
        <div class="" style="color: #000; margin-top:10px;">
            Tipo de publicação: {{d['tipo_publi']}}
        </div>
    %end

    %if d['tipo_pub_onde_foi_publicado'] or d['volume_numero'] or d.get('pais', '') or d.get('lingua_pub', ''):
        <div class="search-result-publication-type" style="color: #000; margin-top:10px;">
            Publicado em: {{d['tipo_pub_onde_foi_publicado']}}, {{d.get('volume_numero', '')}}, {{d['pais']}} | {{d['lingua_pub']}}
        </div>
    %end

    %if d['ano_publicacao']:
        <!-- d['ano_publicacao'] {{d['ano_publicacao']}} -->
        <div class="search-result-date">
            {{d['ano_publicacao']}}
        </div>
    %elif d['time']:
        <!-- d['time'] {{d['time']}} -->
        <div class="search-result-date">
            {{d['time']}}
        </div>
    %end

    %if len(d['disponivel_em']) > 0:
        <div style="font-size: 16px; color: #000; margin-top: 10px;">
            Link para publicação: <a href="{{d['disponivel_em']}}">Link Original</a>
        </div>
    %end
    %if d.get('resp_inserir', ''):
        <div style="margin-top: 10px;">
            Metadados inseridos por: {{d.get('resp_inserir', '')}}
        </div>
    %end
    %if d.get('indicado_por', ''):
        <div style="margin-top: 10px;">
            Indicado por: {{d.get('indicado_por', '')}}
        </div>
    %end

    <hr/>

    %if d['autores_formatados']:
        <h3 style="display: block; margin-top: 10px; font-size: 20px;">Informações complementares dos autores:</h3>
        <ul style="margin-top: 20px;">
            %for autor_detalhes in d['autores_formatados']:
                <li style="margin-top: 10px;">{{autor_detalhes[0]}}: {{autor_detalhes[1]}}, {{autor_detalhes[2]}} | {{autor_detalhes[3]}} | {{autor_detalhes[4]}}</li>
            %end
            </ul>
        <hr/>
    %end

    %if d.get('resumo', ''):
        <h3 style="display: block; font-size: 20px;">Resumo</h3>
        <div class="search-result-resumo" style="margin-top: 10px;">{{d.get('resumo', '')}}</div>
    %end

    %if d.get('snippet', ''):
        <h3 style="display: block; font-size: 20px; margin-top: 10px;">Snippet</h3>
        <!-- Renderizar como HTML Safe -->
        <div class="search-result-snippet" style="margin-top: 10px; font-weight: normal;">{{!d['snippet']}}</div>
    %end

</div>
<!-- vim: fdm=marker:tw=80:ts=4:sw=4:sts=4:et:ai
-->
