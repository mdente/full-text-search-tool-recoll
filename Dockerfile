FROM debian:sid
MAINTAINER vmesel

RUN apt-get update --allow-insecure-repositories --allow-unauthenticated
RUN apt-get install gpg -y
RUN apt-get upgrade -y

RUN gpg --keyserver pool.sks-keyservers.net --recv-key F8E3347256922A8AE767605B7808CE96D38B9201
RUN gpg --export '7808CE96D38B9201' | apt-key add -

RUN echo deb [trusted=yes] http://www.lesbonscomptes.com/recoll/debian/ stable main > \
	/etc/apt/sources.list.d/recoll.list

RUN echo deb-src [trusted=yes] http://www.lesbonscomptes.com/recoll/debian/ stable main >> \
	/etc/apt/sources.list.d/recoll.list

RUN apt-get install -y recoll python3-recoll python3 python3-pip git wv poppler-utils --allow-unauthenticated&& \
    apt-get clean

RUN apt-get install -y apt-utils libcairo2-dev libjpeg-dev libgif-dev pkg-config libimage-exiftool-perl

RUN apt-get install -y unzip xsltproc unrtf untex libimage-exiftool-perl antiword python3-waitress

RUN apt-get install -y vim python3-setuptools python3-dev python-cairo libgirepository1.0-dev

RUN mkdir /homes && mkdir /root/.recoll && mkdir /redalint && mkdir /lantri

COPY ./custom_metadata/requirements.txt /redalint/requirements.txt

WORKDIR /redalint

RUN pip3 install -r requirements.txt

COPY . /redalint

RUN chmod +x setup_index.sh && chmod +x setup_metadata.sh

# Depende do volume /homes/
# RUN ln -s /homes/acervo_redalint /lantri/acervo_redalint

# Isso aqui também, depende do volume montado (/root)
# COPY ./recoll.conf /root/.recoll/recoll.conf

# Depende do volume montado
# RUN python3 metadata_gen.py

# Depende do volume montado
# RUN recollindex

ENTRYPOINT ["gunicorn", "--bind", "0.0.0.0:80", "webui-wsgi:app"]
